# -*- coding:utf-8 -*-
"""
Load data.
Train and test CycleGAN.
"""
from tensorflow.python.ops import array_ops
import os
import logging
import time
from datetime import datetime
import tensorflow as tf
import model.unet_lotus as unet_lotus
import model.unet_lotus_glore as unet_lotus_glore
from model.utils import save_images

import sys
sys.path.append("../data")
from data import read_tfrecords
# load test image
import numpy as np
import cv2
import glob
def iou_core(true,pred):  #this can be used as a loss if you make it negative
        intersection = true * pred
        notTrue = 1 - true
        union = true + (notTrue * pred)
        return (tf.reduce_sum(intersection) + tf.keras.backend.epsilon()) / (tf.reduce_sum(union) + tf.keras.backend.epsilon())
def focal_loss(prediction_tensor, target_tensor, weights=None, alpha=0.25, gamma=2):
     sigmoid_p = tf.nn.sigmoid(prediction_tensor)
     zeros = array_ops.zeros_like(sigmoid_p, dtype=sigmoid_p.dtype)
     pos_p_sub = array_ops.where(target_tensor > zeros, target_tensor - sigmoid_p, zeros)
     neg_p_sub = array_ops.where(target_tensor > zeros, zeros, sigmoid_p)
     per_entry_cross_ent = - alpha * (pos_p_sub ** gamma) * tf.log(tf.clip_by_value(sigmoid_p, 1e-8, 1.0))- (1 - alpha) * (neg_p_sub ** gamma) * tf.log(tf.clip_by_value(1.0 - sigmoid_p, 1e-8, 1.0))
     return tf.reduce_sum(per_entry_cross_ent)
def image_aug(image, mask):
    concat_image = tf.concat([image, mask], axis=-1)
    random_crop = tf.image.random_crop(concat_image, (512, 512))
    maybe_flipped = tf.image.random_flip_left_right(concat_image)
    maybe_flipped = tf.image.random_flip_up_down(maybe_flipped)
    image = maybe_flipped[:, :, :-1]
    mask = maybe_flipped[:, :, -1:]
    image = tf.image.random_brightness(image, 0.7)
    image = tf.image.random_hue(image, 0.3)
    mask = tf.image.resize_images(mask,[324, 324], method=tf.image.ResizeMethod.BICUBIC)
    return image, mask

class UNet(object):
    def __init__(self, sess, output_dir, phase, training_set,  testing_set, model_name):
        self.sess = sess
        self.dtype = tf.float32

        self.output_dir = output_dir
        self.model_name =model_name
        if (self.model_name=='unet'):

            self.checkpoint_dir = os.path.join(self.output_dir, "checkpoint_unet")
            self.saver_name = "checkpoint_unet"
            self.summary_dir = os.path.join(self.output_dir, "summary_unet")
        else:
            self.checkpoint_dir = os.path.join(self.output_dir, "checkpoint_glore")
            self.saver_name = "checkpoint_glore"
            self.summary_dir = os.path.join(self.output_dir, "summary_glore")

        self.checkpoint_prefix = "model"
        self.is_training = (phase == "train")
        self.learning_rate =2e-4

        # data parameters
        self.image_w = 512
        self.image_h = 512 # The raw and mask image is 1918 * 1280.
        self.image_c = 1 # Gray image.

        self.input_data = tf.placeholder(self.dtype, [None, self.image_h, self.image_w, 
            self.image_c])
        self.input_masks = tf.placeholder(self.dtype, [None, 254, 254,
            self.image_c])
        # TODO: The shape of image masks. Refer to the Unet in model.py, the output image is
        # 324 * 324 * 1. But is not good.
        # learning rate
        self.lr = tf.placeholder(self.dtype)

        # train
        if self.is_training:
            self.training_set = training_set
            self.sample_dir = "train_results"

            # makedir aux dir
            self._make_aux_dirs()
            # compute and define loss
            self._build_training()
            # logging, only use in training
            log_file = self.output_dir + "/Unet.log"
            logging.basicConfig(format='%(asctime)s [%(levelname)s] %(message)s',
                                filename=log_file,
                                level=logging.DEBUG,
                                filemode='w')
            logging.getLogger().addHandler(logging.StreamHandler())
        else:
            # test
            self.testing_set = testing_set
            # build model
            self.output = self._build_test()

    def _build_training(self):
        # Unet
        if self.model_name=='unet':
            self.output = unet_lotus.Unet(name="UNet", in_data=self.input_data, reuse=False)
        else:
            self.output = unet_lotus_glore.Unet(name="UNet", in_data=self.input_data, reuse=False)
        self.loss = tf.reduce_mean(tf.keras.losses.binary_crossentropy(
            self.input_masks, self.output))
        #self.loss = focal_loss(self.output, self.input_masks)
        #print('input mask and output', self.input_masks.get_shape(), self.output.get_shape(),self.output).get_shape())
        self.iou = iou_core(self.input_masks, self.output)
        correct_prediction = tf.equal(self.input_masks,tf.round(self.output))
        self.acc = tf.reduce_mean(tf.cast(correct_prediction,tf.float32))
        # optimizer
        print('iou',self.iou.get_shape())
        self.opt = tf.train.AdamOptimizer(learning_rate=self.lr).minimize(
            self.loss, name="opt")
        
        # summary
        tf.summary.scalar('loss', self.loss)
        tf.summary.scalar('acc', self.acc)
        tf.summary.scalar('iou', self.iou)
        self.summary = tf.summary.merge_all()
        # summary and checkpoint
        self.writer = tf.summary.FileWriter(
            self.summary_dir, graph=self.sess.graph)
        self.saver = tf.train.Saver(max_to_keep=10, name=self.saver_name)
        self.summary_proto = tf.Summary()


    def train(self, batch_size, training_steps, summary_steps, checkpoint_steps, save_steps):
        step_num = 0
        # restore last checkpoint
        latest_checkpoint = "model_output/checkpoint_crop/model-210000"
        # use pretrained model, it can be self.checkpoint_dir, "", or you can appoint the saved checkpoint path.
        latest_checkpoint = False # "model_output/checkpoint/model-1500"
        if latest_checkpoint:
            step_num = int(os.path.basename(latest_checkpoint).split("-")[1])
            assert step_num > 0, "Please ensure checkpoint format is model-*.*."
            self.saver.restore(self.sess, latest_checkpoint)
            logging.info("{}: Resume training from step {}. Loaded checkpoint {}".format(datetime.now(), 
                step_num, latest_checkpoint))
        else:
            self.sess.run(tf.global_variables_initializer()) # init all variables
            logging.info("{}: Init new training".format(datetime.now()))

        # Utilize TFRecord file to load data. change the tfrecords name for different datasets.
        # define class Read_TFRecords object.
        tf_reader = read_tfrecords.Read_TFRecords(filename=os.path.join(self.training_set, 
            "table.tfrecords"),
            batch_size=batch_size, image_h=self.image_h, image_w=self.image_w, 
            image_c=self.image_c)
        print("_________________--data_______________",os.path.join(self.training_set,'table.tfrecords')) 

        images, images_masks = tf_reader.read()
        #images, images_masks = image_aug(images, images_masks)
        logging.info("{}: Done init data generators".format(datetime.now()))

        self.coord = tf.train.Coordinator()
        threads = tf.train.start_queue_runners(sess=self.sess, coord=self.coord)

        try:
            # train
            print('start train')
            c_time = time.time()
            lrval = self.learning_rate
            for c_step in range(step_num + 1, training_steps + 1):
                # learning rate, adjust lr
                if c_step % 50000 == 0:
                    lrval = self.learning_rate * .5
                batch_images, batch_images_masks = self.sess.run([images, images_masks])
                #np.save('checkdata/batch_img'+str(c_time)+'.npy',batch_images)
                #np.save('checkdata/batch_mask'+str(c_time)+'.npy', batch_images_masks)
                c_feed_dict = {
                    # TFRecord
                    self.input_data: batch_images,
                    self.input_masks: batch_images_masks,
                    self.lr: lrval
                }
                self.sess.run(self.opt, feed_dict=c_feed_dict)
                # save summary
                if c_step % summary_steps == 0:
                    c_summary = self.sess.run(self.summary, feed_dict=c_feed_dict)
                    self.writer.add_summary(c_summary, c_step)

                    e_time = time.time() - c_time
                    time_periter = e_time / summary_steps
                    logging.info("{}: Iteration_{} ({:.4f}s/iter) {}".format(
                        datetime.now(), c_step, time_periter,
                        self._print_summary(c_summary)))
                    c_time = time.time() # update time

                # save checkpoint
                if c_step % checkpoint_steps == 0:
                    self.saver.save(self.sess,
                        os.path.join(self.checkpoint_dir, self.checkpoint_prefix),
                        global_step=c_step)
                    logging.info("{}: Iteration_{} Saved checkpoint".format(
                        datetime.now(), c_step))

                if c_step % save_steps == 0:
                    img = tf.image.resize(self.input_data, (254,254))
                    image, output_masks, input_masks = self.sess.run(
                        [img, self.output, self.input_masks],
                        feed_dict=c_feed_dict)
                    save_images(None, output_masks, input_masks,
                        input_path = './{}/input_{:04d}.png'.format(self.sample_dir, c_step),
                        image_path = './{}/train_{:04d}.png'.format(self.sample_dir, c_step))
        except KeyboardInterrupt:
            print('Interrupted')
            self.coord.request_stop()
        except Exception as e:
            self.coord.request_stop(e)
        finally:
            # When done, ask the threads to stop.
            self.coord.request_stop()
            self.coord.join(threads)

        logging.info("{}: Done training".format(datetime.now()))

    def _build_test(self):
        # network.
        if (self.model_name=='unet'):
            output = unet_lotus.Unet(name="UNet", in_data=self.input_data, reuse=False)
        else:
            output = unet_lotus_glore.Unet(name="UNet", in_data=self.input_data, reuse=False)


        self.saver = tf.train.Saver(max_to_keep=10, name=self.saver_name) 
        # define saver, after the network!

        return output

    def load(self, checkpoint_name=None):
        # restore checkpoint
        print("{}: Loading checkpoint...".format(datetime.now())),
        if checkpoint_name:
            # checkpoint = os.path.join(self.checkpoint_dir, checkpoint_name)
            checkpoint = checkpoint_name
            self.saver.restore(self.sess, checkpoint)
            print(" loaded {}".format(checkpoint_name))
        else:
            # restore latest model
            latest_checkpoint = tf.train.latest_checkpoint(
                self.checkpoint_dir)
            if latest_checkpoint:
                self.saver.restore(self.sess, latest_checkpoint)
                print(" loaded {}".format(os.path.basename(latest_checkpoint)))
            else:
                raise IOError(
                    "No checkpoints found in {}".format(self.checkpoint_dir))

    def test(self):
        # Test only in a image.
        image_name = glob.glob(os.path.join(self.testing_set, "*.png"))
        print("name image", image_name)
        # In tensorflow, test image must divide 255.0.
        image = np.reshape(cv2.resize(cv2.imread(image_name[0], 0), 
            (self.image_h, self.image_w)), (1, self.image_h, self.image_w, self.image_c)) / 255.
        # OpenCV load image. the data format is BGR, w.t., (H, W, C). The default load is channel=3.

        print("{}: Done init data generators".format(datetime.now()))

        c_feed_dict = {
            self.input_data: image
        }

        output_masks = self.sess.run(
            self.output, feed_dict=c_feed_dict)

        return image, output_masks
        # image: 1 * 512 * 512 * 1
        # output_masks: 1 * 324 * 342 * 1.
    def test_folder(self, folder_image, folder_mask):
        image_names = os.listdir(folder_image)

        for image_name in image_names:
            file_img = os.path.join(folder_image, image_name)
            image = np.reshape(cv2.resize(cv2.imread(file_img, 0),
                                          (self.image_h, self.image_w)),
                               (1, self.image_h, self.image_w, self.image_c)) / 255.
            c_feed_dict = {
                self.input_data: image
            }
            output_mask = self.sess.run(
                self.output, feed_dict=c_feed_dict)
            file_name = os.path.join(folder_mask, image_name)
            cv2.imwrite(file_name, np.uint8(output_mask[0].clip(0., 1.) * 255.))

        return None

    def test_value(self, folder_image, folder_label):
        image_names = os.listdir(folder_image)
        iou_matrix = []
        acc_matrix = []
        for image_name in image_names:
            file_img = os.path.join(folder_image, image_name)
            image = np.reshape(cv2.resize(cv2.imread(file_img, 0),
                                          (self.image_h, self.image_w)),
                               (1, self.image_h, self.image_w, self.image_c)) / 255.
            file_label = os.path.join(folder_label, image_name)
            label = np.reshape(cv2.resize(cv2.imread(file_label, 0), (254, 254)), (1, 254, 254, self.image_c)) / 255.

            self.iou = iou_core(self.input_masks, tf.round(self.output))
            correct_prediction = tf.equal(self.input_masks, tf.round(self.output))
            self.acc = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

            c_feed_dict = {
                self.input_data: image,
                self.input_masks: label
            }
            iou, acc = self.sess.run(
                [self.iou, self.acc], feed_dict=c_feed_dict)
            iou_matrix.append(iou)
            acc_matrix.append(acc)
        return np.mean(iou_matrix), np.mean(acc_matrix)

    def _make_aux_dirs(self):
        if not os.path.exists(self.summary_dir):
            os.makedirs(self.summary_dir)
        if not os.path.exists(self.checkpoint_dir):
            os.makedirs(self.checkpoint_dir)
        if not os.path.exists(self.sample_dir):
            os.makedirs(self.sample_dir)

    def _print_summary(self, summary_string):
        self.summary_proto.ParseFromString(summary_string)
        result = []
        for val in self.summary_proto.value:
            result.append("({}={})".format(val.tag, val.simple_value))
        return " ".join(result)
