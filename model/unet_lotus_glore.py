import tensorflow as tf
import numpy as np
from GloRe_unit.GloReunit import GloReUnit
def conv2d(input, filters, ksize=4, strides=2):
    conv = tf.layers.conv2d(input, filters, ksize, strides, activation=tf.nn.leaky_relu,
            kernel_initializer = tf.contrib.layers.xavier_initializer())
    return tf.layers.batch_normalization(conv)
def deconv2d(input, filters, ksize=4, strides=2, activation=tf.nn.leaky_relu ):
    return tf.layers.conv2d_transpose(input, filters, ksize, strides, activation=activation)

def Unet(name, in_data, reuse=False, phase=True):
    NF = 64
    print('in data', in_data)
    with tf.variable_scope(name+'downsampling', reuse=reuse):
        conv1 = conv2d(in_data, NF)
        conv2 = conv2d(conv1, NF*2)
        conv3 = conv2d(conv2, NF*4)
        conv4 = conv2d(conv3, NF*8)
        conv5 = conv2d(conv4, NF*8)
        conv6 = conv2d(conv5, NF*8)
        conv7 = conv2d(conv6, NF*8)
        print('conv7',conv7)
        glore = GloReUnit(conv7, out_channel_expand=NF*8)
        glore_unit = glore.processing()
        conv8 = conv2d(glore_unit, NF*8, 1, 1)
        print('conv8', conv8)
        #glore = GloReUnit(conv8)
        #glore_unit = glore.processing()
        print('glore', glore_unit)


    with tf.variable_scope(name+'upsampling', reuse=reuse):
        deconv8 = deconv2d(conv8, NF*8,1,1)
        deconv8  = tf.layers.dropout(deconv8)
        deconv8 = tf.concat([deconv8, conv8], axis=3)

        deconv7 = deconv2d(deconv8, NF*8,1,1)
        deconv7 = tf.layers.dropout(deconv7)
        print('deconv7',deconv7)
        deconv7 = tf.concat([deconv7, conv7], axis=3)

        deconv6 = deconv2d(deconv7, NF*8)
        deconv6 = tf.layers.dropout(deconv6)
        deconv6 = tf.concat([deconv6, conv6], axis=3)

        deconv5 = deconv2d(deconv6, NF*8)
        deconv5 = tf.layers.dropout(deconv5)
        deconv5 = tf.concat([deconv5, conv5], axis=3 )

        deconv4 = deconv2d(deconv5, NF*4)
        deconv4 = tf.layers.dropout(deconv4)
        deconv4 = tf.concat([deconv4, conv4], axis=3)

        deconv3 = deconv2d(deconv4, NF*2)
        deconv3 = tf.layers.dropout(deconv3)
        deconv3 = tf.concat([deconv3, conv3],axis=3)

        deconv2 = deconv2d(deconv3, NF*1)
        deconv2 = tf.layers.dropout(deconv2)
        deconv2 = tf.concat([deconv2, conv2], axis=3)

        deconv1 = deconv2d(deconv2, 1, activation=tf.nn.sigmoid)
    print('out data', deconv1)
    return deconv1
