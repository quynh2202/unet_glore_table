

# -*- coding:utf-8 -*-
"""
Generator and Discriminator network.
"""
import tensorflow as tf
from model import utils

# Define Unet, you can refer to http://blog.csdn.net/u014722627/article/details/60883185 or
# https://github.com/zhixuhao/unet
def Unet(name, in_data, reuse=False, phase=True):
    # Not use BatchNorm or InstanceNorm.
    print('in data', in_data)
    assert in_data is not None
    with tf.variable_scope(name, reuse=reuse):
        # Conv1 + Crop1
        conv1_1 = tf.layers.conv2d(in_data, 64, 3, activation=tf.nn.relu,
            kernel_initializer =  tf.keras.initializers.he_normal())
        bn1 = tf.keras.layers.BatchNormalization()(conv1_1)

            # kernel_initializer = tf.variance_scaling_initializer(scale=2.0)) # Use Xavier init.
        # Arguments: inputs, filters, kernel_size, strides((1, 1)), padding(VALID).
        # Appoint activation, use_bias, kernel_initializer, bias_initializer=tf.zeros_initializer().
        # In Keras's implement, kernel_initializer is he_normal, i.e.
        # mean = 0.0, stddev = sqrt(2 / fan_in).
        '''
        In Tensorflow, you can use tf.keras.initializers.he_normal to implement he_normal, 
        but tf.keras.initializers.he_normal is a function, not a class. So we can not use it
        directly. But, if you look at the implementation of he_normal, it is actually use
        tf.variance_scaling_initializer.

        tf.variance_scaling_initializer class. Initializer capable of adapting its scale to the shape of weights tensors.
        Args:
        scale: Scaling factor (positive float). default 1.
        mode: One of "fan_in", "fan_out", "fan_avg". default 'fan_in'.
        distribution: Random distribution to use. One of "normal", "uniform". default "normal".
        seed: A Python integer. Used to create random seeds.

        With distribution="normal", from a normal distribution mean zero, with stddev = sqrt(scale / n), where n is
        the number of input units in the weight tensor, if mode = "fan_in".
        the number of output units, if mode = "fan_out".
        average of the numbers of input and output units, if mode = "fan_avg".

        With distribution="uniform", from a uniform distribution within [-limit, limit], 
        with limit = sqrt(3 * scale / n).

        So, we can set kernel_initializer = tf.variance_scaling_initializer(scale=2.0)
        '''

        conv1_2 = tf.layers.conv2d(bn1, 64, 3, activation=tf.nn.relu,
            kernel_initializer = tf.keras.initializers.he_normal())
        # bn2 = tf.contrib.layers.batch_norm(conv1_2,
        #                                   center=True, scale=True,
        #                                   is_training=phase)
        bn2 = tf.keras.layers.BatchNormalization()(conv1_2)
            # kernel_initializer = tf.variance_scaling_initializer(scale=2.0))
        crop1 = tf.keras.layers.Cropping2D(cropping=((90, 90), (90, 90)))(bn2)
        '''
        Use Tensorflow and Keras, in Tensorflow there are some Keras's APIS, you can use 
        Tensorflow and Keras at the same time. For example:
        x = tf.keras.layers.Dense(128, activation='relu')(input)

        tf.keras.layers.Cropping2D, class Cropping layer for 2D input. Arguments:
        cropping: int, or tuple of 2 ints, or tuple of 2 tuples of 2 ints.
            int: cropping is applied to width and height.
            (symmetric_height_crop, symmetric_width_crop): cropping values for height and width
            ((top_crop, bottom_crop), (left_crop, right_crop))
        First, define a object of class, then use object(data) to forward.
        '''

        # MaxPooling1 + Conv2 + Crop2
        pool1 = tf.layers.max_pooling2d(bn2, 2, 2)
        # Arguments: inputs, pool_size(integer or tuple), strides(integer or tuple),
        # padding='valid'.
        conv2_1 = tf.layers.conv2d(pool1, 128, 3, activation=tf.nn.relu,
            kernel_initializer = tf.keras.initializers.he_normal() )

        bn2_1 = tf.keras.layers.BatchNormalization()(conv2_1)
            # kernel_initializer = tf.variance_scaling_initializer(scale=2.0))

        conv2_2 = tf.layers.conv2d(bn2_1, 128, 3, activation=tf.nn.relu,
            kernel_initializer = tf.keras.initializers.he_normal())
        bn2_2 = tf.contrib.layers.batch_norm(conv2_2,
                                          center=True, scale=True,
                                          is_training=phase)
            # kernel_initializer = tf.variance_scaling_initializer(scale=2.0))
        crop2 = tf.keras.layers.Cropping2D(cropping=((41, 41), (41, 41)))(bn2_2)

        # MaxPooling2 + Conv3 + Crop3
        pool2 = tf.layers.max_pooling2d(bn2_2, 2, 2)
        # Arguments: inputs, pool_size(integer or tuple), strides(integer or tuple),
        # padding='valid'.
        conv3_1 = tf.layers.conv2d(pool2, 256, 3, activation=tf.nn.relu,
            kernel_initializer = tf.keras.initializers.he_normal())
            # kernel_initializer = tf.variance_scaling_initializer(scale=2.0))
        bn3_1  = tf.keras.layers.BatchNormalization()(conv3_1)
        conv3_2 = tf.layers.conv2d(bn3_1, 256, 3, activation=tf.nn.relu,
            kernel_initializer = tf.keras.initializers.he_normal())
            # kernel_initializer = tf.variance_scaling_initializer(scale=2.0))
        bn3_2 = tf.keras.layers.BatchNormalization()(conv3_2)
        crop3 = tf.keras.layers.Cropping2D(cropping=((16, 17), (16, 17)))(bn3_2)

        # MaxPooling3 + Conv4 + Drop4 + Crop4
        pool3 = tf.layers.max_pooling2d(conv3_2, 2, 2)
        # Arguments: inputs, pool_size(integer or tuple), strides(integer or tuple),
        # padding='valid'.
        conv4_1 = tf.layers.conv2d(pool3, 512, 3, activation=tf.nn.relu,
            kernel_initializer = tf.keras.initializers.he_normal())
        bn4_1 = tf.keras.layers.BatchNormalization()(conv4_1)
            # kernel_initializer = tf.variance_scaling_initializer(scale=2.0))
        conv4_2 = tf.layers.conv2d(bn4_1, 512, 3, activation=tf.nn.relu,
            kernel_initializer =tf.keras.initializers.he_normal())
        bn4_2 = tf.keras.layers.BatchNormalization()(conv4_2)
            # kernel_initializer = tf.variance_scaling_initializer(scale=2.0))
        drop4 = tf.layers.dropout(bn4_2)
        # Arguments: inputs, rate=0.5.
        crop4 = tf.keras.layers.Cropping2D(cropping=((4, 4), (4, 4)))(drop4)

        # MaxPooling4 + Conv5 + Crop5
        pool4 = tf.layers.max_pooling2d(drop4, 2, 2)
        # Arguments: inputs, pool_size(integer or tuple), strides(integer or tuple),
        # padding='valid'.
        conv5_1 = tf.layers.conv2d(pool4, 1024, 3, activation=tf.nn.relu,
            kernel_initializer = tf.keras.initializers.he_normal())
        bn5_1 = tf.keras.layers.BatchNormalization()(conv5_1)
            # kernel_initializer = tf.variance_scaling_initializer(scale=2.0))
        conv5_2 = tf.layers.conv2d(bn5_1, 1024, 3, activation=tf.nn.relu,
            kernel_initializer = tf.keras.initializers.he_normal())
        bn5_2 = tf.keras.layers.BatchNormalization()(conv5_2)
            # kernel_initializer = tf.variance_scaling_initializer(scale=2.0))
        drop5 = tf.layers.dropout(bn5_2)

        # Upsampling6 + Conv + Merge6
        up6 = tf.layers.conv2d_transpose(drop5,512, 4,2)
        print('up6')
        merge6 = tf.concat([crop4, up6], axis=3) # concat channel

        # Conv6 + Upsampling7 + Conv + Merge7

        '''
        conv6_1 = tf.layers.conv2d(merge6, 512, 3, activation=tf.nn.relu,
            kernel_initializer =tf.keras.initializers.he_normal())
        bn6_1 = tf.contrib.layers.batch_norm(conv6_1,
                                           center=True, scale=True,
                                           is_training=phase)
            # kernel_initializer = tf.variance_scaling_initializer(scale=2.0))
        conv6_2 = tf.layers.conv2d(bn6_1, 512, 3, activation=tf.nn.relu,
            kernel_initializer = tf.keras.initializers.he_normal())
            # kernel_initializer = tf.variance_scaling_initializer(scale=2.0))
        bn6_2 = tf.keras.layers.BatchNormalization()(conv6_2)
        # up7_1 = tf.keras.layers.UpSampling2D(size=(2, 2))(bn6_2)
        up7_1 = conv2d_dilat(bn6_2, out_filter=512, kernel_size=3, padding="SAME", dilation=2)
        
        
        up7 = tf.layers.conv2d(up7_1, 256, 2, padding="SAME", activation=tf.nn.relu,
            kernel_initializer = tf.keras.initializers.he_normal())'''
        up7  = tf.layers.conv2d_transpose(merge6, 256, 4,2)
            # kernel_initializer = tf.variance_scaling_initializer(scale=2.0))
        merge7 = tf.concat([crop3, up7], axis=3) # concat channel

        # Conv7 + Upsampling8 + Conv + Merge8
        '''conv7_1 = tf.layers.conv2d(merge7, 256, 3, activation=tf.nn.relu,
            kernel_initializer = tf.keras.initializers.he_normal())
        bn7_1 = tf.keras.layers.BatchNormalization()(conv7_1)
            # kernel_initializer = tf.variance_scaling_initializer(scale=2.0))
        conv7_2 = tf.layers.conv2d(bn7_1, 256, 3, activation=tf.nn.relu,
            kernel_initializer = tf.keras.initializers.he_normal())
        bn7_2 = tf.keras.layers.BatchNormalization()(conv7_2)
            # kernel_initializer = tf.variance_scaling_initializer(scale=2.0))

        # up8_1 = tf.keras.layers.UpSampling2D(size=(2, 2))(bn7_2)
        up8_1 = conv2d_dilat(bn7_2, out_filter=128, kernel_size=3, padding="SAME", dilation=2)
        up8 = tf.layers.conv2d(up8_1, 128, 2, padding="SAME", activation=tf.nn.relu,
            kernel_initializer = tf.keras.initializers.he_normal())'''
            # kernel_initializer = tf.variance_scaling_initializer(scale=2.0))

        up8 = tf.layers.conv2d_transpose(merge7,128, 4,2)
        merge8 = tf.concat([crop2, up8], axis=3) # concat channel

        # Conv8 + Upsampling9 + Conv + Merge9
        '''conv8_1 = tf.layers.conv2d(merge8, 128, 3, activation=tf.nn.relu,
            kernel_initializer = tf.keras.initializers.he_normal())
            # kernel_initializer = tf.variance_scaling_initializer(scale=2.0))
        bn8_1 = tf.keras.layers.BatchNormalization()(conv8_1)
        conv8_2 = tf.layers.conv2d(bn8_1, 128, 3, activation=tf.nn.relu,
            kernel_initializer = tf.keras.initializers.he_normal())
            # kernel_initializer = tf.variance_scaling_initializer(scale=2.0))
        bn8_2 = tf.keras.layers.BatchNormalization()(conv8_2)
        # up9_1 = tf.keras.layers.UpSampling2D(size=(2, 2))(bn8_2)
        up9_1 = conv2d_dilat(bn8_2, out_filter=1024, kernel_size=3, padding="SAME", dilation=2)
        up9 = tf.layers.conv2d(up9_1, 64, 2, padding="SAME", activation=tf.nn.relu,
            kernel_initializer = tf.keras.initializers.he_normal())
            # kernel_initializer = tf.variance_scaling_initializer(scale=2.0))'''
        up9 = tf.layers.conv2d_transpose(merge8, 64, 4,2)
        merge9 = tf.concat([crop1, up9], axis=3) # concat channel

        # Conv9
        conv9_1 = tf.layers.conv2d(merge9, 64, 3, activation=tf.nn.relu,
            kernel_initializer = tf.keras.initializers.he_normal())
        bn9_1 = tf.keras.layers.BatchNormalization()(conv9_1)

            # kernel_initializer = tf.variance_scaling_initializer(scale=2.0))
        conv9_2 = tf.layers.conv2d(bn9_1, 64, 3, activation=tf.nn.relu,
            kernel_initializer = tf.keras.initializers.he_normal())
        bn9_2 = tf.keras.layers.BatchNormalization()(conv9_2)
            # kernel_initializer = tf.variance_scaling_initializer(scale=2.0))
        conv9_3 = tf.layers.conv2d(bn9_2, 2, 3, padding="SAME", activation=tf.nn.relu,
            kernel_initializer = tf.keras.initializers.he_normal())
        bn9_3 = tf.keras.layers.BatchNormalization()(conv9_3)

            # kernel_initializer = tf.variance_scaling_initializer(scale=2.0))

        # Conv10
        conv10 = tf.layers.conv2d(bn9_3, 1, 1, activation=tf.nn.sigmoid,
            kernel_initializer = tf.keras.initializers.he_normal())
            # kernel_initializer = tf.variance_scaling_initializer(scale=2.0))
        # 1 channel.

    return conv10

def conv2d_dilat(x, out_filter, kernel_size, padding="SAME", dilation=1, name=None):
    in_filter = int(x.get_shape()[-1])

    filters = tf.get_variable(name="DW" + str(name),
                              dtype=tf.float32,
                              initializer=tf.truncated_normal(
                                  shape=[kernel_size, kernel_size, in_filter, out_filter], stddev=0.01, mean=0.0))

    out_put = tf.nn.atrous_conv2d(value=x, filters=filters, rate=[dilation, dilation], padding=padding)

    return out_put

# def deconv(x, size, stride, out_chanel, output_shape=None):
#     in_chanel = int(x.get_shape()[-1])
#     output_shape =
#     filter = tf.get_variable("w_trans" + str(size),
#                              initializer=tf.truncated_normal([size, size, out_chanel, in_chanel], mean=0.0,
#                                                              stddev=0.01, dtype=tf.float32))
#     # tf.summary.histogram("")
#     out_put = tf.nn.conv2d_transpose(value=x, filter=filter, output_shape=output_shape,
#                                      strides=[1, stride, stride, 1], padding="SAME")
#     return out_put
