import tensorflow as tf

def conv2d(input, filters, ksize, strides=1):
    conv = tf.layers.conv2d(input, filters, ksize, strides, activation=tf.nn.leaky_relu,
            kernel_initializer = tf.contrib.layers.xavier_initializer())
    return tf.layers.batch_normalization(conv)
def conv1d(input, filters, ksize, strides=1 ):
    conv1d_channel = tf.layers.conv1d(input, filters, 1, activation=tf.nn.relu,
                                 kernel_initializer=tf.contrib.layers.xavier_initializer())
    return tf.layers.batch_normalization(conv1d_channel)
class GloReUnit():
    def __init__(self, features, out_channel_reduce=256, out_channel_expand=512, out_nodes=512):
        self.features = features
        self.out_channel_reduce = out_channel_reduce
        self.out_channel_expand = out_channel_expand
        self.number_nodes = out_nodes

    def mul_matrix(self, B_transpose, X):
        B_reshape = tf.reshape(B_transpose, (-1,B_transpose.get_shape()[1].value, B_transpose.get_shape()[2].value*B_transpose.get_shape()[3].value))
        X_reshape = tf.reshape(X, (-1, X.get_shape()[1].value* X.get_shape()[2].value, X.get_shape()[3].value))
        result = tf.matmul(B_reshape, X_reshape)
        return result
    def coordinate_to_interation(self, name=None):
        with tf.variable_scope(name, default_name="coordinate_to_interation"):
            # X_reduced = tf.layers.conv2d(self.features, self.out_channel_reduce, 1, activation=tf.nn.relu,
            #                              kernel_initializer=tf.contrib.layers.xavier_initializer())
            X_reduced = conv2d(self.features, self.out_channel_reduce, 1)
            self.B = tf.layers.conv2d(self.features, self.number_nodes, 1, activation=tf.nn.sigmoid,
                                         kernel_initializer=tf.contrib.layers.xavier_initializer())
            B_transpose = tf.transpose(self.B, (0, 3, 1, 2))
            self.V = self.mul_matrix(B_transpose, X_reduced)

    def graph_convolutional(self, name='conv-gcnn'):

        channel_new =  self.V.get_shape()[1].value
        node_new = self.V.get_shape()[2].value
        V_transpose = tf.transpose(self.V, (0,2,1))
        # conv1d_channel = tf.layers.conv1d(V_transpose, channel_new, 1, activation=tf.nn.relu,
        #                              kernel_initializer=tf.contrib.layers.xavier_initializer())
        conv1d_channel = conv1d(V_transpose, channel_new,1)
        # conv1d_channel = self.V - conv1d_channel
        conv1d_channel = tf.transpose(conv1d_channel, (0, 2, 1))

        # self.conv_graph = tf.layers.conv1d(conv1d_channel, node_new, 1, activation=tf.nn.relu,
        #                              kernel_initializer=tf.contrib.layers.xavier_initializer())
        self.conv_graph = conv1d(conv1d_channel, node_new,1)

    def mul_matrix_second(self, D, Z):
        '''

        :param D: transpose B
        :param Z: nodes
        :return: multiply
        '''
        D_reshape = tf.reshape(D,(-1, D.get_shape()[1].value * D.get_shape()[2].value, D.get_shape()[3].value))
        result = tf.matmul(D_reshape, Z)
        result = tf.reshape(result, (-1, D.get_shape()[1].value, D.get_shape()[2].value, result.get_shape()[2].value ))
        return result

    def interaction_to_coordinate(self, name=None):
        '''
        projection space
        :param name:
        :return:
        '''
        with tf.variable_scope(name, default_name="coordinate_to_interation"):
            self.Y = self.mul_matrix_second(self.B, self.conv_graph)
            # Y_expand = tf.layers.conv2d(self.Y, self.out_channel_expand, 1, activation=tf.nn.relu,
            # kernel_initializer = tf.contrib.layers.xavier_initializer())
            Y_expand = conv2d(self.Y,self.out_channel_expand,1)
        return Y_expand

    def processing(self):
        self.coordinate_to_interation()
        self.graph_convolutional()
        return self.interaction_to_coordinate()



 # def graph_convolution(self, name=None):
    #     initializer = tf.constant_initializer(0.1)
    #     A = tf.get_variable(name, (self.out_dimension_reduce, self.out_dimension_reduce), initializer=initializer, dtype=tf.float32)
    #     self.conv_graph = make_graphcnn_layer(self.V, A, self.no_filters, name=None)
    #     # if with_bn:
    #     # self.make_batchnorm_layer()
    #     # if with_act_func:
    #     # self.current_V = tf.nn.relu(self.current_V)
    #     # if self.network_debug:
    #     # batch_mean, batch_var = tf.nn.moments(self.current_V, np.arange(len(self.current_V.get_shape())-1))
    #     # self.current_V = tf.Print(self.current_V, [tf.shape(self.current_V), batch_mean, batch_var], message='"%s" V Sh
    #     return self.conv_graph

# def coordinate_to_interation(self, name=None):
#     with tf.variable_scope(name, default_name="coordinate_to_interation") as scope:
#         X_reduced = tf.layers.conv2d(self.features, self.out_channel_reduce, 1, activation=tf.nn.relu,
#             kernel_initializer = tf.contrib.layers.xavier_initializer())
#         self.B = tf.layers.conv2d(self.features, self.number_nodes, 1, activation=tf.nn.relu,
#             kernel_initializer = tf.contrib.layers.xavier_initializer())
#         self.V = self.mul_matrix(X_reduced)
# def graph_convolutional(self, name='conv-gcnn', channel_new=32, node_new=32):
#     conv1d_channel = tf.layers.conv1d(self.V, channel_new, 1, name=name+'_channel')
#     conv1d_channel = tf.transpose(conv1d_channel, (0, 2, 1))
#     conv1d_node = tf.layers.conv1d(conv1d_channel, node_new, name=name+'_node' )
#     self.conv_graph = tf.transpose(conv1d_node, (0, 2, 1))
#
#
# def interaction_to_coordinate(self, name=None):
#     with tf.variable_scope(name, default_name="coordinate_to_interation") as scope:
#         self.Y = self.mul_matrix(self.conv_graph)
#         Y_expand = tf.layers.conv2d(self.Y, self.out_channel_expand, 1, activation=tf.nn.relu, kernel_initializer=tf.contrib.layers.xavier_initializer())
#     return Y_expand
