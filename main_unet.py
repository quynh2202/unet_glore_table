# -*- coding:utf-8 -*-
"""
An implementation of CycleGan using TensorFlow (work in progress).
"""
import tensorflow as tf
import numpy as np
from model import unet
import cv2
import argparse
import scipy.misc  # save image


def main():
    param = args()
    batch_size = param.batch_size
    training_steps = param.training_steps
    save_steps = param.save_steps
    summary_steps = param.summary_steps
    checkpoint_steps = param.checkpoint_steps
    checkpoint = param.checkpoint
    model_name = param.model_name
    phase = param.phase
    test_value = param.test_value
    output_dir, training_set, testing_set = param.output_dir, param.training_set, param.testing_set
    # gpu config.
    config = tf.ConfigProto()
    config.gpu_options.per_process_gpu_memory_fraction = 0.5
    config.gpu_options.allow_growth = True

    if phase == "train":
        with tf.Session(config=config) as sess:
            # when use queue to load data, not use with to define sess
            train_model = unet.UNet(sess, output_dir, phase, training_set, testing_set, model_name)
            print("__________________-done model_______________________________")
            train_model.train(batch_size, training_steps,
                              summary_steps, checkpoint_steps, save_steps)
            print("______________________--done train_______________________-")
    elif phase == 'test':

        folder_image = 'data_table_detect/image_test'
        folder_predict = 'data_table_detect/label_predict_unet'
        with tf.Session(config=config) as sess:
            # output_dir, training_set, testing_set = param.output_dir, param.training_set, param.testing_set
            # test on a image pair.
            # checkpoint = 'model_out/unet_model/model-40000'
            test_model = unet.UNet(sess, output_dir='', phase='test', training_set=None, testing_set='', model_name=model_name)
            test_model.load(checkpoint)
            test_model.test_folder(folder_image, folder_predict)

    if (test_value):
        folder_image = 'data_table_detect/image_test'
        folder_label = 'data_table_detect/label_test'
        with tf.Session(config=config) as sess:
            # output_dir, training_set, testing_set = param.output_dir, param.training_set, param.testing_set
            # test on a image pair.
            # checkpoint = 'model_out/unet_model/model-40000'
            test_model = unet.UNet(sess, output_dir='', phase='test', training_set=None, testing_set='', model_name=model_name)
            test_model.load(checkpoint)
            print("______________________________________done load model___________________-")
            iou, acc = test_model.test_value(folder_image, folder_label)
        print('iou:', iou)
        print('acc:', acc)



def args():
    parse = argparse.ArgumentParser(description='descript train end predict model')
    parse.add_argument('--output_dir', default='model_output',
                       help='checkpoint and summary')
    parse.add_argument('--phase', default='train', help="model phase: train/test.")
    parse.add_argument('--training_set', default="tfdata_final/", help="dataset path for training.")
    parse.add_argument('--testing_set', default="test")
    parse.add_argument("--batch_size", default=2)
    parse.add_argument("--training_steps", default=50000)
    parse.add_argument("--summary_steps", default=500,
                       help="summary period.")
    parse.add_argument("--checkpoint_steps", default=1000,
                       help="checkpoint period.")
    parse.add_argument("--save_steps", default=500,
                       help="checkpoint period.")
    parse.add_argument("--checkpoint", default='model_out/unet_model/model-40000',
                       help="checkpoint name for restoring.")
    parse.add_argument('--model_name', default='unet',
    help='model name unet/ unet_glore')
    parse.add_argument('--test_value', default=True,
                       help='test acc iou test data')
    return parse.parse_args()


if __name__ == '__main__':
    main()
