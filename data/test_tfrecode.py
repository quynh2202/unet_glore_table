from read_tfrecords import Read_TFRecords
import cv2
import numpy as np
filename = '../../Unet-Tensorflow-master/data_table_detect/tfdata/table.tfrecords'
filename = '/home/rnd/daisy/Unet-Tensorflow-master/data_table_detect/tfdata/table.tfrecords'
read = Read_TFRecords( filename, batch_size=5,
        image_h=200, image_w=200, image_c=1)
img, mask = read.read()
import tensorflow as tf
print('start run')
with tf.Session() as sess:
    img_, mask_ = sess.run([img, mask])
    np.save('../checkdata/img1.pny', img_[0])
    np.save('../checkdata/img2.pny', img_[1])
    np.save('../checkdata/img3.pny', img_[2])
    np.save('../checkdata/img4.pny', img_[3])
    np.save('../checkdata/img5.pny', img_[4])

    np.save('../checkdata/mask1.pny', mask_[0])
    np.save('../checkdata/mask2.pny', mask_[1])
    np.save('../checkdata/mask3.pny', mask_[2])
    np.save('../checkdata/mask4.pny', mask_[3])
    np.save('../checkdata/mask5.pny', mask_[4])
# print('end run')
# print(img_)
# cv2.imshow(img_[0])
# cv2.waitKey(0)
# cv2.imshow(mask_[0])
# cv2.waitKey(0)
