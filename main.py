# -*- coding:utf-8 -*-
"""
An implementation of CycleGan using TensorFlow (work in progress).
"""
import tensorflow as tf
import numpy as np
from model import unet
import cv2
import argparse
import scipy.misc # save image


def main():
    param = args()
    batch_size = param.batch_size
    training_steps = param.training_steps
    save_steps = param.save_steps
    summary_steps = param.summary_steps
    checkpoint_steps = param.checkpoint_steps
    checkpoint = param.checkpoint
    phase = param.phase
    output_dir, training_set, testing_set = param.output_dir, param.training_set, param.testing_set
    # gpu config.
    config = tf.ConfigProto()
    config.gpu_options.per_process_gpu_memory_fraction = 0.5
    config.gpu_options.allow_growth = True

    if phase == "train":
        with tf.Session(config=config) as sess: 
        # when use queue to load data, not use with to define sess
            train_model = unet.UNet(sess,  output_dir, phase, training_set,  testing_set)
            print("__________________-done model_______________________________")
            train_model.train(batch_size, training_steps,
                              summary_steps, checkpoint_steps, save_steps)
            print("______________________--done train_______________________-")
    else:
        with tf.Session(config=config) as sess:
            # test on a image pair.
            test_model = unet.UNet(sess,  output_dir, phase, training_set,  testing_set)
            test_model.load(checkpoint)
            print("______________________________________done load model___________________-")
            image, output_masks = test_model.test()
            # return numpy ndarray.
            
            # save two images.
            filename_A = "input.png"
            filename_B = "output_masks.png"

            cv2.imwrite(filename_A, np.uint8(image[0].clip(0., 1.) * 255.))
            cv2.imwrite(filename_B, np.uint8(output_masks[0].clip(0., 1.) * 255.))

            # Utilize cv2.imwrite() to save images.
            print("Saved files: {}, {}".format(filename_A, filename_B))
def args():
    parse = argparse.ArgumentParser(description='descript train end predict model')
    parse.add_argument('--output_dir',default = 'model_output',
                       help='checkpoint and summary')
    parse.add_argument('--phase', default='train', help="model phase: train/test.")
    parse.add_argument('--training_set', default="tfdata_final", help="dataset path for training.")
    parse.add_argument('--testing_set', default="test")
    parse.add_argument("--batch_size", default=2)
    parse.add_argument("--training_steps", default=225000)
    parse.add_argument("--summary_steps", default=500,
                                help="summary period.")
    parse.add_argument("--checkpoint_steps", default=1000,
                                        help="checkpoint period.")
    parse.add_argument("--save_steps", default=500,
                                help="checkpoint period.")
    parse.add_argument("--checkpoint", default='',
                               help="checkpoint name for restoring.")
    return parse.parse_args()

if __name__ == '__main__':
    main()
