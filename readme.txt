train model unet:
python3 main_unet.py --phase=train
test unet:
python3 main_unet.py --phase=test --test_value=True --checkpoint=model_out/unet_model/model-40000


train model unet_glore:
python3 main_unet_glore.py --phase=train
test unet:
python3 main_unet_glore.py --phase=test --test_value=True --checkpoint=model_out/Glore_model/model-40000


